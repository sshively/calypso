#!/usr/bin/env python

import sys
from AthenaCommon.Logging import log
from AthenaCommon.Constants import DEBUG
from AthenaCommon.Configurable import Configurable
from CalypsoConfiguration.AllConfigFlags import ConfigFlags
from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from MCEvents.MCEventsConfig import MCEventsCfg


log.setLevel(DEBUG)
Configurable.configurableRun3Behavior = True

# Configure
ConfigFlags.Input.Files = ['my.RDO.pool.root']
ConfigFlags.Output.ESDFileName = "MCEvents.ESD.root"
ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-01"
ConfigFlags.GeoModel.Align.Dynamic = False
ConfigFlags.Beam.NumberOfCollisions = 0.
ConfigFlags.lock()

acc = MainServicesCfg(ConfigFlags)
acc.merge(PoolReadCfg(ConfigFlags))
acc.merge(MCEventsCfg(ConfigFlags))
acc.getEventAlgo("MCEventsAlg").OutputLevel = DEBUG

sc = acc.run(maxEvents=10)
sys.exit(not sc.isSuccess())
