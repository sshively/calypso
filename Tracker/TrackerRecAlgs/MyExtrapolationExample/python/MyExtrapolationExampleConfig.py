"""
    Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
"""

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from FaserSCT_GeoModel.FaserSCT_GeoModelConfig import FaserSCT_GeometryCfg
from MagFieldServices.MagFieldServicesConfig import MagneticFieldSvcCfg

# def FaserActsTrackingGeometrySvcCfg(flags, **kwargs):
#     acc = ComponentAccumulator()
#     FaserActsTrackingGeometrySvc = CompFactory.FaserActsTrackingGeometrySvc
#     acc.addService(FaserActsTrackingGeometrySvc(name="FaserActsTrackingGeometrySvc", **kwargs))
#     return acc

# def FaserActsAlignmentCondAlgCfg(flags, **kwargs):
#     acc = ComponentAccumulator()
#     acc.addCondAlgo(CompFactory.FaserActsAlignmentCondAlg(name="FaserActsAlignmentCondAlg", **kwargs))
#     return acc

def MyExtrapolationExampleCfg(flags, **kwargs):
    acc = FaserSCT_GeometryCfg(flags)
    acc.merge(MagneticFieldSvcCfg(flags))
    # acc.merge(FaserActsTrackingGeometrySvcCfg(flags))
    # acc.merge(FaserActsAlignmentCondAlgCfg(flags))

    actsExtrapolationTool = CompFactory.FaserActsExtrapolationTool("FaserActsExtrapolationTool")
    actsExtrapolationTool.MaxSteps = 1000
    actsExtrapolationTool.TrackingGeometryTool = CompFactory.FaserActsTrackingGeometryTool("TrackingGeometryTool")

    my_extrapolation_example = CompFactory.Tracker.MyExtrapolationExample(**kwargs)
    my_extrapolation_example.ExtrapolationTool = actsExtrapolationTool
    acc.addEventAlgo(my_extrapolation_example)
    return acc
