/*
  Copyright (C) 2022 CERN for the benefit of the FASER collaboration
*/

/**
 * @file CaloRecTool.cxx
 * Implementation file for the CaloRecTool.cxx
 * @ author Deion Fellers, 2022
 **/

#include "CaloRecTool.h"

// Constructor
CaloRecTool::CaloRecTool(const std::string& type, const std::string& name, const IInterface* parent) :
  base_class(type, name, parent)
{
}

//--------------------------------------------------------------
StatusCode
CaloRecTool::initialize() {
  ATH_MSG_INFO( name() << "::initalize()" );

  // Set keys to read calibratiion variables from data base
  ATH_CHECK( m_PMT_HV_ReadKey.initialize() );
  ATH_CHECK( m_MIP_ref_ReadKey.initialize() );

  // access and store calo PMT HV gain curves
  HVgaincurves_rootFile = TFile::Open(m_PMT_HV_Gain_Curve_file.value().c_str(),"read");   // open root file that has TF1 gain curves stored

  chan0_HVgaincurve_pntr = HVgaincurves_rootFile->Get<TF1>(m_chan0_HVgaincurve_name.value().c_str()); // make pointers to the gain curves
  chan1_HVgaincurve_pntr = HVgaincurves_rootFile->Get<TF1>(m_chan1_HVgaincurve_name.value().c_str());
  chan2_HVgaincurve_pntr = HVgaincurves_rootFile->Get<TF1>(m_chan2_HVgaincurve_name.value().c_str());
  chan3_HVgaincurve_pntr = HVgaincurves_rootFile->Get<TF1>(m_chan3_HVgaincurve_name.value().c_str());

  m_HVgainCurveMap[0] = *chan0_HVgaincurve_pntr;  // store TF1 objects in an array mapped to the calo channel numbers
  m_HVgainCurveMap[1] = *chan1_HVgaincurve_pntr;
  m_HVgainCurveMap[2] = *chan2_HVgaincurve_pntr;
  m_HVgainCurveMap[3] = *chan3_HVgaincurve_pntr;

  HVgaincurves_rootFile->Close(); // close the root file


  return StatusCode::SUCCESS;
}

//--------------------------------------------------------------
StatusCode
CaloRecTool::finalize() {
  // Print where you are
  return StatusCode::SUCCESS;
}

//--------------------------------------------------------------
TF1 CaloRecTool::get_PMT_HV_curve(int channel) const {
  if (channel <= 4) {
    return m_HVgainCurveMap[channel];
  } else {
    ATH_MSG_WARNING("channel "<<channel<<" is not <= 4 and thus not a calorimeter channel, so no HV gain-curve exists!");
  }
  return TF1("default", "1.0", 0.0, 2000.0);
}

//--------------------------------------------------------------
float CaloRecTool::getHV(const EventContext& ctx, int channel) const {

  ATH_MSG_DEBUG("in getHV("<<channel<<")");

  float HV=0.;

  // Read Cond Handle
  SG::ReadCondHandle<CondAttrListCollection> readHandle{m_PMT_HV_ReadKey, ctx};
  const CondAttrListCollection* readCdo{*readHandle}; 
  if (readCdo==nullptr) {
    ATH_MSG_FATAL("Null pointer to the read conditions object");
    return HV;
  }
  // Get the validitiy range
  EventIDRange rangeW;
  if (not readHandle.range(rangeW)) {
    ATH_MSG_FATAL("Failed to retrieve validity range for " << readHandle.key());
    return HV;
  }
  ATH_MSG_DEBUG("Size of CondAttrListCollection " << readHandle.fullKey() << " readCdo->size()= " << readCdo->size());
  ATH_MSG_DEBUG("Range of input is " << rangeW);

  // Read offset for specific channel
  const CondAttrListCollection::AttributeList& payload{readCdo->attributeList(channel)};

  if (payload.exists("HV") and not payload["HV"].isNull()) {
    HV = payload["HV"].data<float>();
    ATH_MSG_DEBUG("Found digitizer channel " << channel << ", HV as " << HV);
  } else {
    ATH_MSG_WARNING("No valid HV found for channel "<<channel<<"!");
  }

  return HV;

}

float CaloRecTool::getHV(int channel) const {
  const EventContext& ctx(Gaudi::Hive::currentContext());
  return CaloRecTool::getHV(ctx, channel);
}

//----------------------------------------------------------------------
float CaloRecTool::getHV_ref(const EventContext& ctx, int channel) const {

  ATH_MSG_DEBUG("in getHV_ref("<<channel<<")");

  float HV_ref=0.;

  // Read Cond Handle
  SG::ReadCondHandle<CondAttrListCollection> readHandle{m_MIP_ref_ReadKey, ctx};
  const CondAttrListCollection* readCdo{*readHandle};
  if (readCdo==nullptr) {
    ATH_MSG_FATAL("Null pointer to the read conditions object");
    return HV_ref;
  }
  // Get the validitiy range
  EventIDRange rangeW;
  if (not readHandle.range(rangeW)) {
    ATH_MSG_FATAL("Failed to retrieve validity range for " << readHandle.key());
    return HV_ref;
  }
  ATH_MSG_DEBUG("Size of CondAttrListCollection " << readHandle.fullKey() << " readCdo->size()= " << readCdo->size());
  ATH_MSG_DEBUG("Range of input is " << rangeW);

  // Read offset for specific channel
  const CondAttrListCollection::AttributeList& payload{readCdo->attributeList(channel)};

  if (payload.exists("HV_ref") and not payload["HV_ref"].isNull()) {
    HV_ref = payload["HV_ref"].data<float>();
    ATH_MSG_DEBUG("Found digitizer channel " << channel << ", HV_ref as " << HV_ref);
  } else {
    ATH_MSG_WARNING("No valid HV_ref found for channel "<<channel<<"!");
  }

  return HV_ref;

}

float CaloRecTool::getHV_ref(int channel) const {
  const EventContext& ctx(Gaudi::Hive::currentContext());
  return CaloRecTool::getHV_ref(ctx, channel);
}

//----------------------------------------------------------------------
float CaloRecTool::getMIPcharge_ref(const EventContext& ctx, int channel) const {

  ATH_MSG_DEBUG("in getMIPcharge_ref("<<channel<<")");

  float MIP_charge_ref =0.;

  // Read Cond Handle
  SG::ReadCondHandle<CondAttrListCollection> readHandle{m_MIP_ref_ReadKey, ctx};
  const CondAttrListCollection* readCdo{*readHandle};
  if (readCdo==nullptr) {
    ATH_MSG_FATAL("Null pointer to the read conditions object");
    return MIP_charge_ref;
  }
  // Get the validitiy range
  EventIDRange rangeW;
  if (not readHandle.range(rangeW)) {
    ATH_MSG_FATAL("Failed to retrieve validity range for " << readHandle.key());
    return MIP_charge_ref;
  }
  ATH_MSG_DEBUG("Size of CondAttrListCollection " << readHandle.fullKey() << " readCdo->size()= " << readCdo->size());
  ATH_MSG_DEBUG("Range of input is " << rangeW);

  // Read offset for specific channel
  const CondAttrListCollection::AttributeList& payload{readCdo->attributeList(channel)};

  if (payload.exists("charge_ref") and not payload["charge_ref"].isNull()) {
    MIP_charge_ref = payload["charge_ref"].data<float>();
    ATH_MSG_DEBUG("Found digitizer channel " << channel << ", MIP_charge_ref as " << MIP_charge_ref);
  } else {
    ATH_MSG_WARNING("No valid MIP_charge_ref found for channel "<<channel<<"!");
  }

  return MIP_charge_ref;

}

float CaloRecTool::getMIPcharge_ref(int channel) const {
  const EventContext& ctx(Gaudi::Hive::currentContext());
  return CaloRecTool::getMIPcharge_ref(ctx, channel);
}

//--------------------------------------------------------------




